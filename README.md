# Spring Data - Diary App

#### SpringJPA, Hibernate and Heroku

Spring Data - Diary App assignment for Experis Academy 2020. 

Website for adding, editing and deleting diary entries. 

**By: Karl Löfquist and Angelina Fransson**

* Karl - Frontend: index, navbar, addentry, editentry, listentries

* Angelina - Backend: DiaryController, DiaryEntry, CommonResponse, Repository

* Pair programmed - javaScript, thymeleaf 

Link to [Heroku app](http://spring-data-diary-app.herokuapp.com/).
