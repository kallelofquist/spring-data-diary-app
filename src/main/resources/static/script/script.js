
/**
*   Authors: Angelina Fransson and Karl Löfquist
*/

/*
*   Function for fetching entry information. When adding and editing, this function reads fields marked with
*   specific id:s to relate them to properties of a DiaryEntry object.
*/
function fetchEntryInfo() {
    let  title = document.getElementById("title").value;
    let  date = document.getElementById("date").value;
    let  textField = document.getElementById("textField").value;
    let  imageURL = document.getElementById("imageURL").value;

    let entry = {
        title : title,
        date : date,
        textField : textField,
        imageURL : imageURL
    };
    return entry;
}

/*
*   Function for updating a diary entry. The function fetches information about the DiaryEntry object and passes it on
*   to the /diary-entry/{id} endpoint for updating. Lastly, reloads the page.
*/
function updateEntry(id) {
    const entry = fetchEntryInfo();

    entry.id = id;

    fetch("/diary-entry/" + entry.id, {
        method: "PATCH",
        body: JSON.stringify(entry),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(() => {
        location.reload()
    })
}

/*
*   Function for creating a diary entry. The function fetches information about the DiaryEntry object and passes it on
*   to the /diary-entry/create-entry endpoint for crating an entry. Lastly, sends the user to the endpoint for displaying
*   all entries.
*/
function createEntry() {
    const entry = fetchEntryInfo()
    fetch("/diary-entry/create-entry", {
        method: "POST",
        body: JSON.stringify(entry),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(() => {
        window.location = "/diary-entry/all";
    })
}

/*
*   Function for deleting a diary entry. It goes to the /diary-entry/{id} endpoint for deleting an entry.
*   Lastly, reloads the page.
*/
function deleteEntry(id) {
    fetch("/diary-entry/" + id, {method: "DELETE"}).then(res => {
        return res.json()
    }).then(() => {
        location.reload()
    });
}