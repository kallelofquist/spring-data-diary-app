package se.experis.springdatadiaryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.springdatadiaryapp.Repositories.Repository;
import se.experis.springdatadiaryapp.model.CommonResponse;
import se.experis.springdatadiaryapp.model.DiaryEntry;

import java.util.*;

/**
 * Author: Angelina Fransson
 * Controller class
 */
@Controller
public class DiaryController {

    @Autowired
    private Repository repo;

    //List that is used in multiple methods
    List <DiaryEntry> entryList = new ArrayList<>();

    //Fall-back if user accidentally reaches this page
    @GetMapping("/diary-entry")
    public ResponseEntity<CommonResponse> diaryRoot() {

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        cr.message = "Not implemented";

        HttpStatus resp = HttpStatus.NOT_IMPLEMENTED;

        return new ResponseEntity<>(cr, resp);
    }


    //When the user clicks on the button "show all entries", the user will reach this endpoint.
    @GetMapping("/diary-entry/all")
    public String getAllEntries(Model model) {
        //find all objects in the repository
        entryList = repo.findAll();
        //sort it after date
        entryList.sort(new DiaryEntry());
        //Adding the "entries" list to thymeleaf
        model.addAttribute("entries", entryList);
        //re-directs to the html page listentries
        return "listentries";
    }

    //When the user clicks on the edit button next to an entry, the user will reach this endpoint
    @GetMapping("/diary-entry/edit/{id}")
    public String editEntry(@PathVariable Integer id, Model model) {
        //find all objects in the repository
        //process
        CommonResponse cr = new CommonResponse();


        //Checking if the entry has a valid id
        if(repo.existsById(id)) {
            Optional<DiaryEntry> repository = repo.findById(id);
            DiaryEntry entry = repository.get();

            cr.data = entry;
            cr.message = "Updated entry with id: " + entry.getId();

            model.addAttribute("entry", entry);

        } else {
            cr.message = "Entry not found with id: " + id;
        }
        //Adding the specific entry to thymelead
        //re-directs to the html page editentry
        return "editentry";
    }

    //When the user clicks on the add entry button, the user will reach this endpoint
    @GetMapping("/diary-entry/add")
    public String addEntry() {
        //Re-directs the user to the html page addentity
        return "addentry";
    }

    //When the user clicks on the save button when creating an entry, the user will reach this endpoint
    @PostMapping("diary-entry/create-entry")
    public ResponseEntity<CommonResponse> createEntry(@RequestBody DiaryEntry entry){
        //if the entry is empty, return null
        if(!validateEntry(entry)) {
            return null;
        }
        //process
        entry = repo.save(entry);

        CommonResponse cr = new CommonResponse();
        cr.data = entry;
        cr.message = "New entry with id: " + entry.getId();

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, resp);
    }

    //If the user clicks on the edit button, the user will reach this endpoint
    @PatchMapping ("/diary-entry/{id}")
    public ResponseEntity<CommonResponse> updateEntry (@RequestBody DiaryEntry newEntry, @PathVariable Integer id) {
        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        //Checking if the entry has a valid id, and if true, check if the fields are empty. If not, set new value
        if(repo.existsById(id)) {
            Optional<DiaryEntry> repository = repo.findById(id);
            DiaryEntry entry = repository.get();

            if(newEntry.getTitle() != null) {
                entry.setTitle(newEntry.getTitle());
            }
            if(newEntry.getDate() != null) {
                entry.setDate(newEntry.getDate());
            }
            if(newEntry.getTextField() != null) {
                entry.setTextField(newEntry.getTextField());
            }
            if(newEntry.getImageURL() != null) {
                entry.setImageURL(newEntry.getImageURL());
            }

            repo.save(entry);

            cr.data = entry;
            cr.message = "Updated entry with id: " + entry.getId();
            resp = HttpStatus.OK;
        } else {
            cr.message = "Entry not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }

    //If the user clicks on the delete button, the user will reach this endpoint
    @DeleteMapping("/diary-entry/{id}")
    public ResponseEntity<CommonResponse> deleteEntry(@PathVariable Integer id){
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        //If the user tries to delete a entry object with an existing id, delete
        if(repo.existsById(id)) {
            repo.deleteById(id);
            cr.message = "Deleted entry with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Entry not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Checks so that no information is empty.
     * @param entry - Entry holding the information to be checked.
     * @return - True if valid else false.
     */
    private boolean validateEntry(DiaryEntry entry) {
        return (notNullOrEmpty(entry.getTitle()) && notNullOrEmpty(entry.getDate()) && notNullOrEmpty(entry.getTextField()));

    }

    private boolean notNullOrEmpty(String s) {
        return s != null && !s.equals("");
    }
}