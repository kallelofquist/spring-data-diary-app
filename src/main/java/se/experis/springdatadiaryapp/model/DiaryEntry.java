package se.experis.springdatadiaryapp.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;
import java.util.Comparator;

/**
 * Author: Angelina Fransson
 * Entity class for Diary entries.
 * Sorts the diary entries after date
 *
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class DiaryEntry implements Comparator<DiaryEntry> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected int id;

    @Column(nullable = false)
    protected String title;

    @Column(nullable = false)
    protected String date;

    @Column(columnDefinition = "text")
    protected String textField;

    @Column
    protected String imageURL;

    public DiaryEntry() {
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getTextField() {
        return textField;
    }

    public int getId() {
        return id;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTextField(String textField) {
        this.textField = textField;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    //Method that sorts all diary entries after date
    @Override
    public int compare(DiaryEntry o1, DiaryEntry o2) {
        return o2.getDate().compareTo(o1.getDate());
    }
}