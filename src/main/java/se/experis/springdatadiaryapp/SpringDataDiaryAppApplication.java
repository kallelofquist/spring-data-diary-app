package se.experis.springdatadiaryapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataDiaryAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataDiaryAppApplication.class, args);
	}

}
