package se.experis.springdatadiaryapp.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.springdatadiaryapp.model.DiaryEntry;

/**
 * Author: Angelina Fransson
 * This class converts objects into database records and vice versa
 */
public interface Repository extends JpaRepository<DiaryEntry, Integer> {
    DiaryEntry getById(String id);

}
